<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Socialite;
use Auth;
use App\User;
use Mail;

class SocialController extends Controller
{
    public function redirect($provider){
     return Socialite::driver($provider)->redirect();
    }

 //    public function Callback($provider){
 //    $userSocial =   Socialite::driver($provider)->user();
	// }

	public function Callback($provider){
        $userSocial =   Socialite::driver($provider)->stateless()->user();
        $users       =   User::where(['email' => $userSocial->getEmail()])->first();
		if($users){
            Auth::login($users);
            return redirect('/');
        }else{
			$user = User::create([
                'name'          => $userSocial->getName(),
                'email'         => $userSocial->getEmail(),
                'image'         => $userSocial->getAvatar(),
                'provider_id'   => $userSocial->getId(),
                'provider'      => $provider,
            ]);
    			Auth::login($user);			

         return redirect()->route('home');
        }
    }

    public function testMail(){
$to_name = 'habib';
				$to_email = 'habib.cst@gmail.com';
				$data = array('name'=>'habib', 'body' => 'A test mail');
				Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email) {
									$message->to($to_email, $to_name)
									->subject('Laravel Test Mail');
									$message->from(env('MAIL_USERNAME'),'Test Mail');
							});

    }

   //  public function Callback($provider){
   //  echo   "Line of Code";
   //  }


}
