@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
        <!--             <div
  class="fb-like"
  data-share="true"
  data-width="450"
  data-show-faces="true">
</div>
      <div class="fb-login-button" data-width="260px" data-max-rows="1" data-size="large"
                                     data-button-type="login_with" data-show-faces="false" data-auto-logout-link="false"
                                     data-use-continue-as="true" scope="email,public_profile"
                                     onlogin=""></div>

                            <form action="http://localhost/gp-white-board/admin/fbLogin" method="post"
                                      id="fbLoginForm" style="display: none">

                                    <input placeholder="" id="fbid" name="fbid" type="text">
                                    <input placeholder="" id="fbname" name="fbname" type="text">
                                    <input placeholder="" id="fbemail" name="fbemail" type="text">
                                </form>
                    <form> -->
<div class="form-group row">
    <div class="col-md-4 offset-md-4">
         <a href="{{ url('/login/facebook') }}" class="btn btn-info btn-block btn-facebook text-light"><i class="fa fa-facebook-square" aria-hidden="true"></i>&nbsp;Facebook</a><br>
         <a href="{{ url('/login/google') }}" class="btn btn-danger btn-block btn-google-plus"><i class="fa fa-google" aria-hidden="true"></i>&nbsp; Google</a>
         
    </div>
</div>
</form>
                    <!-- <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            </div>
                        </div>
                    </form> -->
                </div>
            </div>
        </div>
    </div>
</div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '422528135275596',
      xfbml      : true,
      version    : 'v4.0'
    });
    FB.AppEvents.logPageView();
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
@endsection
